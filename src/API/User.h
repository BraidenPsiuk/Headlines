#pragma once

#include "UI/ListViews/RedditContentListView.h"

namespace api
{
    class User
    {
    public:
        explicit User(Json::Value jsonData);

        const std::string& GetUrl() const { return m_Url; }
        const std::string& GetDisplayNamePrefixed() const { return m_DisplayNamePrefixed; }
        const std::string& GetDisplayName() const { return m_DisplayName; }
        const std::string& GetName() const { return m_Name; }
        int GetKarma() const { return m_Karma; }
        const std::string& GetIconPath() const { return m_IconPath; }
        uint64_t GetCakeDayTimeStamp() const { return m_CakeDayTimeStamp; }
        bool UserIsSubscriber() const { return m_UserIsSubscriber; }

        void SetUserIsSubscriber(bool subscriber);

    private:
        void UpdateDataFromJson(Json::Value jsonValue);
        std::string m_Url;
        std::string m_DisplayNamePrefixed;
        std::string m_DisplayName;
        std::string m_Name;
        int m_Karma = 0;
        std::string m_IconPath;
        uint64_t m_CakeDayTimeStamp = 0;
        bool m_UserIsSubscriber = false;
    };
}