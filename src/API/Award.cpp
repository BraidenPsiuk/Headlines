#include "Award.h"

namespace api
{
    Award::Award(const Json::Value& json)
    {
        //attempt to grab a small icon if available.
        Json::Value resizedIcons = json["resized_icons"];
        if (resizedIcons.isArray())
        {
            for (unsigned int i = 0; i < resizedIcons.size(); ++i)
            {
                Json::Value imageData = resizedIcons[i];
                m_ImageUrl = imageData["url"].asString();

                int width = imageData["width"].asInt();
                if (width >= 32)
                {
                    break;
                }
            }
        }

        if (m_ImageUrl.empty())
        {
            m_ImageUrl = json["icon_url"].asString();
        }


        m_Count = json["count"].asUInt();
        m_Description = json["description"].asString();
        m_Name = json["name"].asString();
    }
}