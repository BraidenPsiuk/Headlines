#pragma once

#include <UI/ui_types_fwd.h>

namespace util
{
    class ThreadWorker;
    class SimpleThread;
}

namespace ui
{
    class RedditContentProvider
    {
    public:
        virtual ~RedditContentProvider() { Reset(); }
        void GetContentAsync(std::function<void(std::vector<RedditContentListItemRef>)> onFinished, std::function<void()> onFailed);
        virtual std::string GetFailedToLoadErrorMsg() = 0;
        bool HasMoreContent() const { return m_HasMoreContent; }
        void Reset();
        bool LoadingContent();
    private:
        std::vector<RedditContentListItemRef> GetContent(util::ThreadWorker* worker, bool* error);
        virtual std::vector<RedditContentListItemRef> GetContentInternal(std::string& lastTimeStamp, util::ThreadWorker* worker) = 0;

        std::string m_LastTimeStamp;
        bool m_HasMoreContent = true;

        util::SimpleThread* m_Thread = nullptr;
        std::mutex m_ThreadMutex;
    };
}

