#pragma once
#include "RedditContentProvider.h"

namespace ui
{
    class PostWidget;
    class UserGildedPostsContentProvider : public RedditContentProvider
    {
    public:
        UserGildedPostsContentProvider();
    private:
        virtual std::vector<RedditContentListItemRef> GetContentInternal(std::string& lastTimeStamp, util::ThreadWorker* worker) override;
        virtual std::string GetFailedToLoadErrorMsg() override;
    };
}
