#pragma once
#include "RedditContentProvider.h"

namespace ui
{
    class PostWidget;
    class AllContentProvider : public RedditContentProvider
    {
    public:
        AllContentProvider();
        virtual ~AllContentProvider() {}

    private:
        virtual std::vector<RedditContentListItemRef> GetContentInternal(std::string& lastTimeStamp, util::ThreadWorker* worker) override;
        virtual std::string GetFailedToLoadErrorMsg() override;
    };
}
