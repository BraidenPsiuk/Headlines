#pragma once
#include "RedditContentProvider.h"
#include "API/api_fwd.h"

namespace ui
{
    class PostWidget;
    class MultiRedditContentProvider : public RedditContentProvider
    {
    public:
        MultiRedditContentProvider(const api::MultiRedditRef& multiReddit);
        virtual ~MultiRedditContentProvider() {}

    private:
        virtual std::vector<RedditContentListItemRef> GetContentInternal(std::string& lastTimeStamp, util::ThreadWorker* worker) override;
        virtual std::string GetFailedToLoadErrorMsg() override;
        api::MultiRedditRef m_MultiReddit;
    };
}

