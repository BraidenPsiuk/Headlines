#pragma once

#include "UI/ui_types_fwd.h"

namespace ui
{
    struct UISettings
    {
        bool m_ShowBackButton;
        bool m_ShowSortButton;
        bool m_ShowSendButton;
        bool m_ShowTitle;
        bool m_ShowWritePostButton;
        bool m_ShowRefreshButton;
        bool m_ShowDownloadButton;
        bool m_ShowSendMessageButton;
    };

    class Page
    {
    public:
        explicit Page(PageType type);
        virtual ~Page() {}
        const std::string& GetID() const;
        PageType GetPageType() const;
        virtual bool IsPopup() const { return false; }

        virtual void CreateUI(AdwLeaflet* parent);
        virtual void Reload() = 0;
        virtual void Cleanup() = 0;
        virtual SortType GetSortType() const = 0;
        virtual std::vector<SortType> GetAllSortTypes() const { return { GetSortType() }; }
        virtual UISettings GetUISettings() const = 0;
        virtual void OnActivate() {}
        virtual void OnDeactivate() {}
        virtual void OnSaveButton() {}
        std::vector<std::pair<std::string, std::string>> GetMenuActions() const { return m_ActionNames; }

        virtual Gtk::Box* GetContentBox() const { return m_Box; }
        const ui::HeaderBarRef& GetHeaderBar() const { return m_HeaderBar; }

    protected:
        AdwLeafletPage* m_LeafletPage = nullptr;

        void AddAction(const std::string& name, const std::function<void()>& cb);

    private:
        virtual Gtk::Box* CreateUIInternal(AdwLeaflet* parent) = 0;
        virtual void OnHeaderBarCreated() {}

        Gtk::Box* m_Box = nullptr;
        std::string m_ID;
        PageType m_PageType;
        ui::HeaderBarRef m_HeaderBar;

        std::vector<std::pair<std::string, std::string>> m_ActionNames;
        int m_ActionIndex = 0;
        std::vector<Glib::RefPtr<Gio::SimpleAction>> m_Actions;
    };
}
