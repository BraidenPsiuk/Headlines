#pragma once

#include <UI/Widgets/VideoPlayer.h>
#include "UI/Pages/Page.h"

namespace ui
{
    class VideoPage : public Page
    {
    public:
        VideoPage(const std::string& title, const std::string videoUrl);

        virtual void Reload() override;
        virtual void Cleanup() override;
        virtual SortType GetSortType() const override;
        virtual UISettings GetUISettings() const override;
        virtual void OnDeactivate() override;

    private:
        virtual void OnHeaderBarCreated() override;
        Gtk::Box* CreateUIInternal(AdwLeaflet* parent) override;

        std::string m_Title;
        std::string m_VideoUrl;
        ui::VideoPlayer* m_VideoPlayer;
    };
}