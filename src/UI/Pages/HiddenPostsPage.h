#pragma once

#include "Page.h"

namespace ui
{
    class HiddenPostsPage : public Page
    {
    public:
        HiddenPostsPage();
        Gtk::Box* CreateUIInternal(AdwLeaflet* parent) override;
        virtual void Cleanup() override;
        virtual void Reload() override;
        virtual SortType GetSortType() const override { return SortType::UserPosts; }
        virtual UISettings GetUISettings() const override;
    private:

        ui::RedditContentListViewRef m_ListView;
    };
}