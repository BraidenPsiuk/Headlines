#include "Application.h"

int main(int argc, char **argv)
{
    Application::Run(argc, argv);
    Application::Destroy();
}