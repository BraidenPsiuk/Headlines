#pragma once

#include "util/Signal.h"
namespace util
{
    class Signals
    {
    public:
        static Signals* Get();

        Signal<bool> LoginStatusChanged;
        Signal<bool> RoundedCornersSettingChanged;
    };

}
