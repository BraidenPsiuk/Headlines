#include "ImageDownloader.h"
#include "SimpleThread.h"
#include "ThreadWorker.h"
#include "Helpers.h"
#include <filesystem>
#include <utility>
#include <curl/curl.h>
#include "ImageCache.h"
#include "HttpClient.h"

namespace util
{
    void ImageDownloader::download_image_async(const std::string& image_url, std::function<void(const std::string&)> on_finished, CancellationToken* cancellation_token)
    {
        if (m_image_download_thread)
        {
            g_warning("Attempting to download an image with an active download thread. aborting.");
            return;
        }

        m_image_url = image_url;

        m_mutex.lock();
        JobCallback job_cb { cancellation_token, std::move(on_finished) };
        m_on_finished.emplace_back(job_cb);
        m_mutex.unlock();

        if (m_image_url.empty())
        {
            return;
        }

        m_image_download_thread = new util::SimpleThread([this](util::ThreadWorker* worker)
        {
            download_image_internal(worker);
            return nullptr;
        },
        [this](void*, bool cancelled, util::SimpleThread*)
        {
            if (!cancelled)
            {
                m_image_download_thread = nullptr;

                m_mutex.lock();
                m_completed = true;
                for (const auto& cb : m_on_finished)
                {
                    if (!cb.m_cancellation_token || !cb.m_cancellation_token->is_cancelled())
                    {
                        cb.m_cb(ImageCache::get_image_disk_path(m_image_url));
                    }

                    if (cb.m_cancellation_token)
                    {
                        delete cb.m_cancellation_token;
                    }

                }
                m_mutex.unlock();
            }
        });
    }

    void ImageDownloader::add_callback(const std::function<void(const std::string&)>& cb, CancellationToken* cancellation_token)
    {
        m_mutex.lock();
        JobCallback job_cb { cancellation_token, cb };
        m_on_finished.emplace_back(job_cb);
        m_mutex.unlock();
    }

    void ImageDownloader::download_image_internal(util::ThreadWorker* worker)
    {
        if (worker->IsCancelled())
        {
            return;
        }

        std::string filePath = ImageCache::get_image_disk_path(m_image_url);

        if (worker->IsCancelled())
        {
            return;
        }

        if (!std::filesystem::exists(filePath))
        {
            util::HttpClient client;
            client.DownloadImage(m_image_url, filePath, {});
        }

        if (worker->IsCancelled())
        {
            return;
        }
    }

    void ImageDownloader::cancel()
    {
        if (m_image_download_thread)
        {
            m_image_download_thread->Cancel();
            m_image_download_thread = nullptr;
        }
    }

    bool ImageDownloader::is_completed()
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        return m_completed;
    }
}
