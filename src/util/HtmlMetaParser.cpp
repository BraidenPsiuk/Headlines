#include <curl/curl.h>
#include "HtmlMetaParser.h"
#include "Helpers.h"

namespace util
{
    std::string get_html_disk_path(const std::string& url)
    {
        if (url.empty())
        {
            return "";
        }

        std::vector<std::string> image_url_split;
        util::Helpers::SplitString(url, image_url_split, '/');
        std::string file_name = image_url_split[image_url_split.size() - 1];

        if (file_name.find('?') != std::string::npos)
        {
            file_name.resize(file_name.find('?'));
        }

        file_name = util::Helpers::FormatString("%s/headlines/%s.html", Glib::get_user_cache_dir().c_str(), file_name.c_str());
        return file_name;
    }

    std::map<std::string, std::string> HtmlMetaParser::get_metadata(const std::string& url)
    {
        std::map<std::string, std::string> ret;

        std::string disk_path = get_html_disk_path(url);
        download_html(url, disk_path);
        htmlDocPtr doc =  htmlReadFile(disk_path.c_str(), NULL, HTML_PARSE_NOERROR | HTML_PARSE_NOWARNING | HTML_PARSE_NOBLANKS | HTML_PARSE_NONET);
        if (doc == nullptr)
        {
            return ret;
        }

        htmlNodePtr cur = doc->children;
        while (cur != NULL)
        {
            if ((cur->type == XML_ELEMENT_NODE) && (cur->name != NULL))
            {
                if (xmlStrEqual(cur->name, BAD_CAST"html"))
                {
                    cur = cur->children;
                    continue;
                }
                if (xmlStrEqual(cur->name, BAD_CAST"head"))
                {
                    cur = cur->children;
                    continue;
                }
                if (xmlStrEqual(cur->name, BAD_CAST"meta"))
                {
                    std::string name;
                    std::string val;

                    auto curr_property = cur->properties;
                    while (curr_property != nullptr)
                    {
                        if (xmlStrEqual(curr_property->name, BAD_CAST"property"))
                        {
                            name = (const char*)curr_property->children->content;
                        }
                        else if (xmlStrEqual(curr_property->name, BAD_CAST"content"))
                        {
                            val = (const char*)curr_property->children->content;
                        }

                        curr_property = curr_property->next;
                    }

                    if (!name.empty() && !val.empty())
                    {
                        ret[name] = val;
                    }
                }
            }
            cur = cur->next;
        }

        return ret;
    }

    size_t download_callback(void* ptr, size_t size, size_t nmemb, void* userdata)
    {
        FILE* stream = (FILE*) userdata;
        if (!stream)
        {
            return 0;
        }

        size_t written = fwrite((FILE*) ptr, size, nmemb, stream);
        return written;
    }

    void HtmlMetaParser::download_html(const std::string& url, const std::string& out_file_name)
    {
        errno = 0;
        FILE* file = fopen(out_file_name.c_str(), "wb");
        if (!file)
        {
            g_warning("failed to open file for writing: %s with error %d", out_file_name.c_str(), errno);
            return;
        }

        CURL* curl = curl_easy_init();
        curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, file);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, download_callback);
        curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_easy_setopt(curl, CURLOPT_USERAGENT, "Headlines/1.0");
        curl_easy_setopt(curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        curl_easy_perform(curl);
        curl_easy_cleanup(curl);

        fclose(file);
    }
}