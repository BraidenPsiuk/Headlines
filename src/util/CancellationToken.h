#pragma once

namespace util
{
    class CancellationToken
    {
    public:
        CancellationToken();
        void cancel();
        bool is_cancelled() const;

    private:
        mutable std::mutex m_mutex;
        bool m_cancelled;
    };
}